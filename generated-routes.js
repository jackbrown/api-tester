module.exports=[
  {
    "route": "/v1/auth/login",
    "method": "get"
  },
  {
    "route": "/v1/auth/sign-up",
    "method": "post"
  },
  {
    "route": "/v1/auth/forgot",
    "method": "get"
  },
  {
    "route": "/v1/auth/validate",
    "method": "post"
  },
  {
    "route": "/v1/auth/profile",
    "method": "get"
  },
  {
    "route": "/v1/person/:personId",
    "method": "patch"
  },
  {
    "route": "/v1/person/:personId",
    "method": "get"
  },
  {
    "route": "/v1/person/:personId",
    "method": "delete"
  }
]
