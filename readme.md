# api-tester

### Installation:
```
npm i @jackbrown9375/api-tester -g
```

### Description:

Command to test all the routes of an api and detect logic or security problems.

**Warning**: It is recommended to use only towards own systems in local deployments,and only to detect and correct existing problems.

**WARNING**: Do not test with systems deployed in production.

**We are not responsible for the misuse of this script.**

### Steps for usage (only 2 steps):
**Step One:** Aggregate a url that exports all our urls in the API that we want to test 
under the path `/test-routes`.
Example of result: 
```json 
 {
        data: [
            {
                "route": "/v1/auth/sign-up",
                "method": "post"
            },
            {
                "route": "/v1/person/:personId",
                "method": "patch"
            }
        ]
    }
```

**Full example (of export urls method) for Express js servers:**

```js
const express = require('express');
const app = express();
const port = 3000;
app.get('/', (req, res) => {
    res.send('Hello World!');
})

var areWeInDevelopment = false;

/**
 *        STARTING OUR LOGIC TO EXPORT ALL ENDPOINTS FROM OUR SERVER
 *
 *--------defining the function `exportExpressRoutes` to export all our endpoints----
*/
function exportExpressRoutes(expressServer, endPointURL, pathsToHide) {
    return expressServer.route(endPointURL)
        .get(function (req, res) {
            var route, routes = [], cleanRoutes = [];
            expressServer._router.stack.forEach(function (middleware) {
                if (middleware.route) {
                    routes.push(middleware.route);
                }
                else if (middleware.name === 'router') {
                    middleware.handle.stack.forEach(function (handler) {
                        route = handler.route;
                        route && routes.push(route);
                    });
                }
            });
            routes.forEach(function (temp) {
                for (var method in temp.methods) {
                    var ruta = {
                        route: temp.path,
                        method: method.toLowerCase(),
                    };
                    if (ruta.route && ruta.route.endsWith != undefined && pathsToHide.indexOf(ruta.route) == -1) {
                        cleanRoutes.push(ruta);
                    }
                }
            });
            return res.status(200).json({
                data: cleanRoutes
            });
        });
}
/**
 we will only export our endpoints if we are in development mode 
*/
if (areWeInDevelopment) {
    exportExpressRoutes(app, '/test-routes', ["/v1/auth/logout", "/"])
}
/**
 *      ENDING OUR LOGIC TO EXPORT ALL OUR SERVER URLS
 * 
*/

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
})
```

**Step two:**
Open your terminal, install `@jackbrown9375/api-tester` globally,
and execute the testing examples bellow:


# Examples:
- To execute all urls without logged in user
    `api-tester URL_API_DOMAIN VERB `
    where URL_API_DOMAIN is the root path of our project
    and verb is one http verb(https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)
In action:
```    
api-tester http://localhost:7890 get
```

-  To consume all urls with logged in user
    api-tester URL_API_DOMAIN JSON_WEB_TOKEN VERB
  where URL_API_DOMAIN is the root path of our project
    and verb is one http verb(https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)
```
api-tester http://localhost:7890 eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c  PATCH
```

# Other Requests: 
    api-tester -k REQUEST_TYPE REQUEST_TYPE_ATTRIBUTES URL [JSON_WEB_TOKEN] VERB

### Request-types: 
```
deepJson
        attributes: 
        `depth`  `number-of-requests-to-make`
        Examples:
            api-tester -k deepJson 9000000  22  http://localhost:9876/comment post
            
            api-tester -k deepJson 9000000  22  http://localhost:9876/comment  eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c post

            Description: It create a json with a 9 million depth
            and perform  22 requests to the url 
            `http: // localhost:9876/comment` for the verb` post`,
            the first without being logged in 
            and the second being logged in the system.
```
```
textLong
        attributes: 
        `attributeName`  `text-length`  `custom-body-stringify` 

        Examples:            
            api-tester -k textLong review 9000000 '{"ProductId":3}'  http://localhost:9876/comment post

            api-tester -k textLong review 9000000 '{"ProductId":3}'  http://localhost:9876/comment eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c post

            Description: Send to register a `comment` with the
            `review` field of 9 million characters to the 
            url `http://localhost:9876/comment` by the verb `post`, 
            the first without being logged in 
            and the second being logged in the system,
            both of them with a stringify body to send.
```
```
DoS
        attributes: 
        `total-requests` `how-many-in-parallel`  `custom-body-stringify`

        Examples:            
            api-tester -k DoS 9000000 5000 ''  http://localhost:8998/v1/product get

            api-tester -k DoS 9000000 5000 '{"id":5}'  http://localhost:8998/v1/product eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c post

            Description: Execute a total of 9 million requests
            to the path `http: //localhost:8998/v1/product`,
            each time 5000 in parallel.
            The first is a `get` without being logged in and without a body.
            The second being logged with a stringify of the body to send.
            (For better use of it, it is recommended to execute the request from
             several [at least 4] terminal at the same time)
```