module.exports = function (parentRoute, childRoute, params, result, error) {
    // console.log("parentRoute,childRoute",global.ii,global.tempRoutes.length);
    // console.log(parentRoute,childRoute)
    var shouldIContinueTheSameRequest = true;
    if (error) {
        console.log(error);
        /**
        * Usualmente este error ocurre cuando el api da hang up,
        * por eso daremos unos milisegundos a ver si el api se recupera
        */
        return new Promise(function (resolve) {
            setTimeout(function () {
                global.ii++;
                resolve(shouldIContinueTheSameRequest);
            }, 10020)
        })
    } else {
        if (result.status >= 200 && result.status <= 399) {
            shouldIContinueTheSameRequest = false;
        }
        return result.text().then(function (data) {
            // if (result.status != 403 && result.status != 404&& result.status != 401) {
                // console.log(parentRoute,childRoute)
                if (global.result[result.status]==undefined){
                    global.result[result.status]={
                        count: 0
                    }
                }

                global.result[result.status].count++;
                console.log("\n"+result.url)
                console.log(result.status, ":::", data)
            // }
        }).then(function () {
            return new Promise(function (resolve) {
                global.ii++;
                resolve(shouldIContinueTheSameRequest);
            })
        })

    }
}