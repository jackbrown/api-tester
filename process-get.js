var fetch = require('node-fetch');
module.exports = function (route, params) {
    global.tempRoute = route;
    global.tempRoutes = global.prepareRoute(route);
    global.ii = 0;
    global.tempParams = params;
    return nextProcess(true)
}

function nextProcess(shouldIContinue) {
    if (shouldIContinue && global.ii < global.tempRoutes.length) {
        var resolved = false;
        // setTimeout(function () {
        //     if (!resolved) {
        //         return global.report(global.tempRoute, global.tempRoutes[global.ii], global.tempParams, null, {
        //             title: "Se partioooooo, nunca devolvio en menos de "+global.maxTimeWaiting
        //         }).then(nextProcess(shouldIContinue))
        //     }
        // }, global.maxTimeWaiting)
        return fetch(global.baseUrl + global.tempRoutes[global.ii] + "?limit=1", global.tempParams).then(function (data) {
            resolved = true;
            return global.report(global.tempRoute, global.tempRoutes[global.ii], global.tempParams, data, null).then(nextProcess(shouldIContinue))
        }).catch(function (error) {
            resolved = true;
            return global.report(global.tempRoute, global.tempRoutes[global.ii], global.tempParams, null, error).then(nextProcess(shouldIContinue))
        })
    } else {
        return new Promise(function (resolve) {
            resolve()
        })
    }
}