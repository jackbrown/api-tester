switch (global.type) {
    case "deepJson":
        return processDeepJson();
    case "denyRegistry":
        return processDenyRegistry();
    case "textLong":
        return processTextLong();
    case "DoS":
        return processDenyOfService();
    default:
        console.log("Lea los ejemplos")
        break;

}


function processDenyOfService() {
    /**
     
    [
        0'/usr/local/bin/node',
        1'api-tester/index.js',
        2-k
        3 DoS 
        4 200000 
        5 9000 
        6 '{"name":"uno"}' 
        7  http://localhost:9876/comment
        8 jwt-for-simple-user 
        9 post
    ]
     */

  
        require("./deny-of-service-by-requests")(global.params)
    

}

function processTextLong() {
    /**
     
    [
        0'/usr/local/bin/node',
        1'api-tester/index.js',
        2-k
        3 textLong 
        4 description 
        5 9000000 
        6 '{"name":"uno"}' 
        7  http://localhost:9876/comment
        8 jwt-for-simple-user 
        9 post
    ]
     */


    require("./text-long")(global.params)


}

function processDenyRegistry() {
    /**
     
    [
        0'/usr/local/bin/node',
        1'api-tester/index.js',
        2'-k',
        3'denyRegistry',

        4'id',
        5'{"comment":"Hola Mundo"}',
        6'http://localhost:8998/v1/product',
        7'post'
    ]
     */

    require("./deny-of-new-registry")(global.params)


}

function processDeepJson() {
    /**
     
    [
        0'/usr/local/bin/node',
        1'api-tester/index.js',
        2'-k',
        3'deepJson',
        4'9000000',
        5'22',
        6'http://localhost:8998/v1/product',
        
        7'post'
    ]
     */

    require("./deepth-json")(global.params)

}