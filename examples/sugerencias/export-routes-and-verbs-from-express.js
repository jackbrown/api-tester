
/**
 * TODO: Si su api esta en express js
 * exponer este codigo en una zona de su api
 * donde tenga acceso al express,
 * en caso contrario debe desde su api crear la ruta `/test-routes`
 * para devolver todas las urls con sus verbos
 */

express.route('/test-routes')
.get(function (req, res) {
  var route, routes = [], cleanRoutes = [];

  express._router.stack.forEach(function (middleware) {
    if (middleware.route) { // routes registered directly on the app
      routes.push(middleware.route);
    }
    else if (middleware.name === 'router') {
      middleware.handle.stack.forEach(function (handler) {
        route = handler.route;
        route && routes.push(route);
      });
    }
  });
  routes.forEach(function (temp) {
    for (var method in temp.methods) {
      var ruta = {
        route: temp.path,
        method: method.toLowerCase(),
      };
      if (ruta.route.endsWith != undefined && ruta.route.endsWith("help") == false
        && ruta.route != "/v1/auth/logout"
      ) {
        cleanRoutes.push(ruta);
      }
    }
  });
  return res.status(200).json({
    data: cleanRoutes
  });
});