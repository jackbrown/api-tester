var fetch = require("node-fetch");
module.exports = function exec(params) {
  var profundidad = params.profundidad;
  var requests = params.requests;
  var method = params.method;
  var Authorization = params.Authorization;

  var a = `{"a":`;
  var jsonString = "";
  var ra = "";
  var rf = "";
  var fin = `}`;
  for (var i = 0; i < profundidad; i++) {
    ra = ra + a;
    rf = rf + fin;
  }
  jsonString = ra + `"Hola"` + rf;


  var options = {
    method: method,
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + Authorization,
    },
    body: jsonString,
  };
  for (var i = 0; i < requests; i++) {
    console.log("Request number:", i);
    fetch(params.url, options).then(function (data) {
      console.log("Response status:", data.status);
    });
  }
}