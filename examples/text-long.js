/**
 * Para chequear si algun campo string es text-long sin limites
 * 
 */



var fetch = require("node-fetch")

/**
 * Este es para demostrar que los usuarios no deben tener acceso a insertar/editar
 * un id de un objeto en una tabla mysql
 *
 * El maximo entero en mysql es Math.pow(2,31)-1 = 2147483647
 *
 * creamos/alteramos un elemento en db para que tenga ese id, y el siguiente
 * para ser insertado en el sistema dará id duplicado *
 */

var fetch = require("node-fetch");
module.exports = function exec(params) {
  var payload = JSON.parse(params.payload);
  payload[params.attributeName] = getTextLong(params.length);;
  payload = JSON.stringify(payload);
  var options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + params.Authorization,
    },
    body: payload,
  };
  console.log(options)
  fetch(params.url, options).then(function (data) {
    console.log("Status: ", data.status);
    data.text().then(function (text) {
      console.log("Response: ", text)
    })
  });
}

function getTextLong(length){
	var textLong = "a";
	while(textLong.length*2<length){
		textLong+=textLong
	}
	var current=textLong.length;
	for (var i=current;i<length;i++){
		textLong+="a";
	}
	return textLong;
}