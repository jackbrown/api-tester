/**
 * Si nuestra api esta montada con una version de apache menor que  2.5.7
 * la denegacion de servicio puede ser aplicada por una sola pc
 * solo con mantener las peticiones abiertas el mayor tiempo posible
 * apache no podra abrir una nueva peticion
 */