var fetch = require("node-fetch");

module.exports = function exec(params) {

  var options = {
    method: params.method,
    headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer " + params.Authorization
    }
  };
  if (params.method.toLowerCase() != "get") {
    options.body = params.payload;
  }
  console.log(options)

  var status = 5;
  var wasAssgined = false;

  function makeRequest(url) {
    return fetch(url, options)
      .then(function (data) {
        if (data.status != 5 && !wasAssgined) {
          status = data.status;
        } else if (data.status != status && data.status != 429) {
          console.log("Exito, status:", status);
        }
      })
      .catch(function (error) {
        console.log("Exito::", error.message);
      });
  }

  var aCasoFunciona = false;
  var start = 0;
  var end = params.totalQueryCount;
  console.log("Total de peticiones: ", end - start);

  function processNext(ii, myEnd) {
    if (ii <= myEnd && aCasoFunciona == false) {
      if (ii % 500 == 0) {
        console.log("ii:", ii);
      }
      return makeRequest(params.url).then(function () {
        ii++;
        processNext(ii, myEnd);
      });
    }
  }

  var countParts = params.parallelCount;
  var total = end - start;
  var part = parseInt(total / countParts);
  var i = 0;
  for (i = 0; i < countParts - 1; i++) {
    processNext(i * part, (i + 1) * part);
  }
  processNext(i * part, end);
}