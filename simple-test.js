

var fetch = require("node-fetch");
var methodParam = "";
var AuthorizationBearer = "";
if (process.argv.length == 3) {
    methodParam = process.argv[2];
} else {
    AuthorizationBearer = process.argv[2];
    methodParam = process.argv[3];
}
if (methodParam == undefined) {
    methodParam = "get"
}
function startTesting(routes) {
    global.report = require("./report-generator");
    global.prepareRoute = require("./prepare-route");
    global.result = {};

    function report() {
        console.log("Total routes of this system:", routes.length);
    }
    report();

    var methods = {
        get: require("./process-get"),
        post: require("./process-get"),
        patch: require("./process-get"),
        delete: require("./process-get"),
        put: require("./process-get"),
    };
    var body = JSON.stringify(require("./full-body-param"));

    function processRequest({ route, method }) {
        var par = {
            method: method,
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + AuthorizationBearer,
            },
        };
        if (method != "get") {
            par.body = body;
        }
        if (
            methodParam.toLowerCase()==method.toLowerCase()
        ) {
            return methods[method](route, par);
        } else {
            return new Promise(function (resolve) {
                resolve();
            });
        }
    }

    global.myii = 0;
    function processNext() {
        if (global.myii < routes.length) {
            return processRequest(routes[global.myii]).then(function () {
                global.myii++;
                return processNext();
            });
        } else {
            return new Promise(function (resolve) {
                resolve();
            });
        }
    }

    processNext().then(function () {
        // console.log("Reporte finalmente");
        // console.log("Reporte finalmente");
        // console.log(JSON.stringify(global.result, null, 2));
    });
}

function processUrl(url, method) {
    fetch(url + "/test-routes").then(function (data) {
        if (data.status == 200) {
            data.json().then(function (dat) {
                startTesting(dat.data);
            });
        } else {
            console.log(
                `The route \`${url}/test-routes\` can not be tested, check the adress`
            );
        }
    });
}

processUrl(global.baseUrl, methodParam);
