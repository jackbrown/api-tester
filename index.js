#!/usr/bin/env node

var fs = require("fs");
var path = require("path")
global.maxTimeWaiting = 1200;
var params = {
  http: "",
  method: "",
  authorization: ""
};
function errorConParametros(){
  console.log("Verifique los parametros:");
  process.exit(0);
}
var wasFound = false;
for (let i = 0; i < process.argv.length && !wasFound; i++) {
  const element = process.argv[i];
  if (element == "-k" || element == "-K") {
    wasFound = true;
    switch (process.argv[i + 1]) {
      case 'textLong':

        if (!(i + 7 == process.argv.length || i + 6 == process.argv.length)) {
          errorConParametros();
        }
        global.type="textLong";
        /**
        3 textLong 
        4 description 
        5 9000000 
        6 '{"name":"uno"}' 
        7  http://localhost:9876/comment
        8 jwt-for-simple-user 
        9 post
       */
        params.attributeName = process.argv[i + 2];
        params.length = process.argv[i + 3];
        params.payload = process.argv[i + 4];
        params.url = process.argv[i + 5];
        params.method = process.argv[i + 6];
        if (process.argv.length == i + 8) {
          params.Authorization = process.argv[i + 6];
          params.method = process.argv[i + 7];
        }
        break;

      case 'deepJson':
        if (!(i + 6 == process.argv.length || i + 7 == process.argv.length)) {
          errorConParametros();
        }

        global.type="deepJson";
        /**
       [
        0'/usr/local/bin/node',
        1'api-tester/index.js',
        2'-k',
        3'deepJson',
        4'9000000',
        5'22',
        6'http://localhost:8998/v1/product',
        7'post'
       ]
       */
        params.profundidad = process.argv[i + 2];
        params.requests = process.argv[i + 3];
        params.url = process.argv[i + 4];
        params.method = process.argv[5];
        if (process.argv.length == i + 7) {
          params.Authorization = process.argv[i + 5];
          params.method = process.argv[i + 6];
        }
        break;
      case 'DoS':
        if (!(i + 7 == process.argv.length || i + 8 == process.argv.length)) {
          errorConParametros();
        }
        global.type="DoS";
        /**
          [
              0'/usr/local/bin/node',
              1'api-tester/index.js',
              2-k
              3 DoS 
              4 200000 
              5 9000 
              6 '{"name":"uno"}' 
              7  http://localhost:9876/comment
              8 jwt-for-simple-user 
              9 post
          ]
          */
        params.totalQueryCount = process.argv[i + 2];
        params.parallelCount = process.argv[i + 3];
        params.payload = process.argv[i + 4];
        params.url = process.argv[i + 5];
        params.method = process.argv[i + 6];
        if (process.argv.length == i + 8) {
          params.Authorization = process.argv[i + 6];
          params.method = process.argv[i + 7];
        }
        break;
      case 'denyRegistry':
        if (!(i + 5 == process.argv.length || i + 6 == process.argv.length)) {
          errorConParametros();
        }

        global.type="denyRegistry";
        /**
           
          [
              0'/usr/local/bin/node',
              1'api-tester/index.js',
              2'-k',
              3'denyRegistry',

              4'id',
              5'{"comment":"Hola Mundo"}',
              6'http://localhost:8998/v1/product',
              7'post'
          ]
          */

        params.attributeName = process.argv[i+2];
        params.payload = process.argv[i+3];
        params.url = process.argv[i+4];
        params.method = process.argv[i+5];
        if (process.argv.length == i+7) {
          params.Authorization = process.argv[i+5];
          params.method = process.argv[i+6];
        }
        break;
      default:
        errorConParametros();
        break;
    }
  }
  if (wasFound) {
    global.params=params;
  }
}
if (!wasFound) {
  for (let i = 0; i < process.argv.length && !wasFound; i++) {
    const element = process.argv[i];
    if (element.startsWith("http")) {
      wasFound = true;
      params["http"] = element;
      params["method"] = process.argv[process.argv.length - 1];
      if (process.argv[i + 1] != params.method) {
        params.authorization = process.argv[i + 1];
      }
    }
  }
  global.is = "http";
} else {


  global.is = "-k";
}

global.baseUrl = params.http;
global.params = params;
if (
  global.baseUrl == undefined ||
  global.baseUrl == "help" ||
  global.baseUrl == "?" ||
  global.baseUrl == "/?"
  || !wasFound
) {
  console.log(":::Debe suministrar una estructura de la siguente manera:::");
  console.log(__dirname)
  var readMe = fs.readFileSync(path.join(__dirname, "readme.md"), "utf8")
  console.log(readMe);
  process.kill(process.pid);
}


if (global.is == "-k") {
  require("./examples/index");
} else
if (global.is == 'http') {
  require("./simple-test");
} else {
  console.log("Verifique los parametros: ", process.argv)
}