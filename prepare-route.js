
global.generatedRoutes = {};
module.exports = function (string) {
    if (global.generatedRoutes[string] != undefined) {
        return global.generatedRoutes[string];
    }
    var parts = string.split("/");
    var results = [];
    for (var i = 0; i < parts.length; i++) {
        if (parts[i][0] == ":") {
            for (var j = 1; j < 15; j++) {
                var curLength = results.length
                for (var k = 0; k < curLength; k++) {
                    var x = results[k].split(parts[i]).join(j);
                    if (results.indexOf(x) == -1) {
                        results.push(x);
                    }
                }
            }
        } else {
            results.push(string);
        }
    }
    var finalResult = []
    for (var i = 0; i < results.length; i++) {
        if (results[i].indexOf(":") == -1 && finalResult.indexOf(results[i]) == -1) {
            finalResult.push(results[i]);
        }
    }
    global.generatedRoutes[string] = finalResult;
    return finalResult;
}
