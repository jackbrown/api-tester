var fs = require("fs");
exports.start = function (express) {
  var route, routes = [], cleanRoutes = [];

  express._router.stack.forEach(function (middleware) {
    if (middleware.route) { // routes registered directly on the app
      routes.push(middleware.route);
    }
    else if (middleware.name === 'router') {
      middleware.handle.stack.forEach(function (handler) {
        route = handler.route;
        route && routes.push(route);
      });
    }
  });
  var c = 0;
  routes.forEach(function (temp) {
    for (var method in temp.methods) {
      c += 1;
      var ruta = {
        route: temp.path,
        method: method.toLowerCase(),
      };
      if (ruta.route.endsWith!=undefined && ruta.route.endsWith("help") == false 
      && ruta.route!="/v1/auth/logout"
      ) {
        cleanRoutes.push(ruta);
      }
    }
  });
  fs.writeFileSync('/media/zxc/503955a7-f448-417b-80d0-ad67b661fa0b/seguridad-final/generated-routes.js', "module.exports=" + JSON.stringify(cleanRoutes, null, 2));
}
