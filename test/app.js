const express = require('express');
const app = express();

const port = 3000;

app.get('/', (req, res) => {
    res.send('Hello World!');
})


var areWeInDevelopment = false;
function exportExpressRoutes(expressServer, route, pathsToAvoid) {
    return expressServer.route(route)
        .get(function (req, res) {
            var route, routes = [], cleanRoutes = [];

            expressServer._router.stack.forEach(function (middleware) {
                if (middleware.route) {
                    routes.push(middleware.route);
                }
                else if (middleware.name === 'router') {
                    middleware.handle.stack.forEach(function (handler) {
                        route = handler.route;
                        route && routes.push(route);
                    });
                }
            });
            routes.forEach(function (temp) {
                for (var method in temp.methods) {
                    var ruta = {
                        route: temp.path,
                        method: method.toLowerCase(),
                    };
                    if (
                        ruta.route &&
                        ruta.route.endsWith != undefined &&
                        pathsToAvoid.indexOf(ruta.route) == -1
                    ) {
                        cleanRoutes.push(ruta);
                    }
                }
            });
            return res.status(200).json({
                data: cleanRoutes
            });
        });
}
if (areWeInDevelopment) {
    exportExpressRoutes(app, '/test-routes', ["/v1/auth/logout", "/"])
}

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
})

